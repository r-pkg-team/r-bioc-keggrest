Package: KEGGREST
Version: 1.46.0
Title: Client-side REST access to the Kyoto Encyclopedia of Genes and
        Genomes (KEGG)
Authors@R: c(
    person("Dan", "Tenenbaum", role = "aut"),
    person("Bioconductor Package", "Maintainer", role = c("aut", "cre"),
           email = "maintainer@bioconductor.org"),
    person("Martin", "Morgan", role = "ctb"),
    person("Kozo", "Nishida", role = "ctb"),
    person("Marcel", "Ramos", role = "ctb"),
    person("Kristina", "Riemer", role = "ctb"),
    person("Lori", "Shepherd", role = "ctb"),
    person("Jeremy", "Volkening", role = "ctb")
    )
Depends: R (>= 3.5.0)
Imports: methods, httr, png, Biostrings
Suggests: RUnit, BiocGenerics, BiocStyle, knitr, markdown
Description: 
    A package that provides a client interface to the Kyoto
    Encyclopedia of Genes and Genomes (KEGG) REST API. Only
    for academic use by academic users belonging to academic
    institutions (see <https://www.kegg.jp/kegg/rest/>).
    Note that KEGGREST is based on KEGGSOAP by J. Zhang, R. Gentleman,
    and Marc Carlson, and KEGG (python package) by Aurelien Mazurie.
URL: https://bioconductor.org/packages/KEGGREST
BugReports: https://github.com/Bioconductor/KEGGREST/issues
License: Artistic-2.0
VignetteBuilder: knitr
biocViews: Annotation, Pathways, ThirdPartyClient, KEGG
RoxygenNote: 7.1.1
Date: 2024-06-17
git_url: https://git.bioconductor.org/packages/KEGGREST
git_branch: RELEASE_3_20
git_last_commit: b34820c
git_last_commit_date: 2024-10-29
Repository: Bioconductor 3.20
Date/Publication: 2024-10-29
NeedsCompilation: no
Packaged: 2024-10-30 01:19:41 UTC; biocbuild
Author: Dan Tenenbaum [aut],
  Bioconductor Package Maintainer [aut, cre],
  Martin Morgan [ctb],
  Kozo Nishida [ctb],
  Marcel Ramos [ctb],
  Kristina Riemer [ctb],
  Lori Shepherd [ctb],
  Jeremy Volkening [ctb]
Maintainer: Bioconductor Package Maintainer <maintainer@bioconductor.org>
